object Constants:
  var acceleration = 0.1
  var maxSpeed = 20.0
  var friction = 1.0
  var gravity = 0.0

  val ranges = Map[String, (Double, Double)](
    "Acceleration" -> (0.001, 4),
    "Max speed" -> (1, 30),
    "Friction" -> (1, 0.9),
    "Gravity" -> (0, 0.5)
  )

  def update(label: String, value: Double) =
    label match
      case "Acceleration" => acceleration = value
      case "Max speed" => maxSpeed = value
      case "Friction" => friction = value
      case "Gravity" => gravity = value
