import Dir.{East, North, South, West}
import java.awt.geom.Rectangle2D
import scala.swing.Panel

class PhysicsRectangle2D(val mass: Double, var loc: V2D, width: Int, val panel: Panel):
  val velocity = new V2D(0, 0)
  var acceleration = new V2D(0, 0)

  def dimensions(loc: V2D) = Dimensions(loc.x.toInt, loc.x.toInt + width, loc.y.toInt, loc.y.toInt + width)

  def wouldCollide: Boolean =
    val cd = collisionDirections
    cd._1.isDefined || cd._2.isDefined

  def collisionDirections: (Option[Dir], Option[Dir]) = // 1st for hor, 2nd for ver
    var hor: Option[Dir] = None
    var ver: Option[Dir] = None
    val size = //getDefaultToolkit.getScreenSize
      panel.size
    val screenWidth = size.width
    val screenHeight = size.height
    val dummyLoc = this.loc + this.velocity + this.acceleration
    val dim = dimensions(dummyLoc)
    if dim.minX < 0 then
      hor = Some(West)
    else if dim.maxX > screenWidth then
      hor = Some(East)
    if dim.minY < 0 then
      ver = Some(North)
    else if dim.maxY > screenHeight then
      ver = Some(South)

    (hor, ver)


  def accelerate(): Unit =
    velocity += acceleration

  def move(): Unit =
    val cd = collisionDirections
    Array(cd._1, cd._2).foreach(oDir => if oDir.isDefined then bounce(oDir.get))
    (loc += velocity)
    val xSign = if loc.x < 0 then -1 else 1
    val ySign = if loc.y < 0 then -1 else 1
    //velocity.x = math.min(math.abs(velocity.x), Constants.maxSpeed) * xSign
    //velocity.y = math.min(math.abs(velocity.y), Constants.maxSpeed) * ySign

  def friction(): Unit =
    velocity *= Constants.friction

  def gravity(): Unit =
    velocity.y += Constants.gravity

  def bounce(dir: Dir): Unit = // 0 for horizontal, 1 for vertical
    dir match
      case West | East => velocity.x *= -1
      case North | South => velocity.y *= -1

  def shape: java.awt.geom.Rectangle2D =
    new Rectangle2D.Double(loc.x, loc.y, width, width)

  def update(): Unit =
    accelerate()
    friction()
    gravity()
    move()
