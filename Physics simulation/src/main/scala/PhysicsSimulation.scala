import scala.swing.{BoxPanel, Dimension, Graphics2D, Label, MainFrame, Orientation, Panel, SimpleSwingApplication}
import java.awt.{AlphaComposite, BasicStroke, Color, Font, Graphics2D, Point, RenderingHints}
import java.awt.image.BufferedImage
import java.awt.geom.{Point2D, Rectangle2D}
import java.awt.Toolkit.getDefaultToolkit
import javax.swing.UIManager
import scala.swing.event.{FocusLost, KeyPressed, KeyReleased, MouseDragged, MouseEvent, MousePressed, MouseReleased}
import java.awt.event.KeyEvent.{VK_DOWN, VK_LEFT, VK_RIGHT, VK_UP}
import scala.swing.event.ValueChanged

enum Dir:
  case North
  case East
  case South
  case West

import Dir._

case class Dimensions(minX: Int, maxX: Int, minY: Int, maxY: Int)

object PhysicsSimulation extends SimpleSwingApplication {

  val mf = new MainFrame {

    title = "Physics simulation"
    resizable = true

    val sliderLabels = Array[SliderLabel](
      new SliderLabel("Acceleration", 0, 1000, 50),
      new SliderLabel("Gravity", 0, 1000, 0),
      new SliderLabel("Friction", 0, 1000, 0)
    )

    val dataWindow: Panel = new Panel {

      focusable = true

      listenTo(mouse.clicks, mouse.moves, keys)

      sliderLabels.foreach( sp => listenTo( sp.slider ) )

      reactions += {
        case e: KeyPressed =>
          val acc = Constants.acceleration
          e.peer.getKeyCode match
            case VK_RIGHT =>
              sq.acceleration.x = acc
            case VK_LEFT =>
              sq.acceleration.x = -acc
            case VK_UP =>
              sq.acceleration.y = -acc
            case VK_DOWN =>
              sq.acceleration.y = acc
            case _ =>
        case e: KeyReleased =>
          e.peer.getKeyCode match
            case VK_RIGHT =>
              if sq.acceleration.x > 0 then sq.acceleration.x = 0
            case VK_UP =>
              if sq.acceleration.y < 0 then sq.acceleration.y = 0
            case VK_LEFT =>
              if sq.acceleration.x < 0 then sq.acceleration.x = 0
            case VK_DOWN =>
              if sq.acceleration.y > 0 then sq.acceleration.y = 0
            case _ =>

        case ValueChanged(source) =>
          val s = source.asInstanceOf[swing.Slider]
          sliderLabels.find( _.slider == s ).get.updateSubject()

        case _: FocusLost => repaint()
      }

      override def paintChildren(g: Graphics2D): Unit = {

        // Set anti-aliasing
        g.setRenderingHint(
          RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON)
        g.setRenderingHint(
          RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON)

        g.setColor(Color.BLACK)
        g.fill(sq.shape)
        val testDim = sq.dimensions(sq.loc)
        val rounded = Array(sq.velocity.x, sq.velocity.y).map(BigDecimal(_).setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble)
        g.drawString("velocity x: " + rounded.head, 100, 120)
        g.drawString("velocity y: " + rounded(1), 100, 140)

      }

    }

    val sq = new PhysicsRectangle2D(50, V2D(400, 400), 50, dataWindow)

    def update(): Unit =
      sq.update()
      //if sliderLabels.length == 3 then println(sliderLabels.head)
      this.repaint()

    object TickListener extends java.awt.event.ActionListener :
      def actionPerformed(e: java.awt.event.ActionEvent): Unit =
        update()

    val timer = javax.swing.Timer(0, TickListener)
    timer.start()

    contents = new BoxPanel(Orientation.Vertical) {
      contents += dataWindow
      contents += new BoxPanel(Orientation.Horizontal) {
        sliderLabels.foreach( sl => contents += sl.panel )
      }
    }

    this.maximize()

    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName)

  }

  def top = mf


}









