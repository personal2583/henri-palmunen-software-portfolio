import scala.swing.{BoxPanel, Label, Orientation}

class SliderLabel(label: String, _min: Int, _max: Int, _value: Int):
  val defaultBorder = new javax.swing.border.EmptyBorder(10, 10, 10, 10)
  val slider = new scala.swing.Slider() {
    border = defaultBorder
    max = _max
    min = _min
    value = _value
    enabled = true
    focusable = false
    //value = _value
  }
  val panel = new BoxPanel(Orientation.Vertical) {
    contents += slider
    contents += new Label(label) { border = defaultBorder }
  }
  def getValue = slider.value
  def updateSubject(): Unit =
    val range = Constants.ranges(label)
    val test1 = getValue / 1000.0
    val test2 = (range._2 - range._1)
    val test3 = getValue / 1000.0 * (range._2 - range._1)
    Constants.update(label, range._1 + getValue / 1000.0 * (range._2 - range._1) )
