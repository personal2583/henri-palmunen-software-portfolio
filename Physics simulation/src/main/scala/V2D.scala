
class V2D(var x: Double, var y: Double):
  def +=(other: V2D): Unit =
    x += other.x
    y += other.y

  def +(other: V2D): V2D =
    new V2D(x + other.x, y + other.y)

  def *=(c: Double): Unit =
    x *= c
    y *= c
