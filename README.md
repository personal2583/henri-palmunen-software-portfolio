Hi,

Welcome to (the start of) my programming portfolio. Here you'll find a data visualization program and a simple physics simulation written in Scala.

To open sbt (Scala) projects, follow the steps [here](https://docs.scala-lang.org/getting-started/sbt-track/getting-started-with-scala-and-sbt-on-the-command-line.html) (command line) or [here](https://www.jetbrains.com/help/idea/sbt-support.html) (IntelliJ, recommended). E.g to open the simulation, open the folder "Physics simulation" in the IntelliJ file picker.

Cheers,

Henri Palmunen
henri.palmunen@aalto.fi
